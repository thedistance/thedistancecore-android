package uk.co.thedistance.thedistancecore;

import android.annotation.SuppressLint;
import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Build;
import android.os.Environment;
import android.provider.DocumentsContract;
import android.provider.MediaStore;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;

/**
 * A collection of methods to help with the handling of images
 */
public class ImageUtils {

    private interface Decoder {
        Bitmap decode(BitmapFactory.Options options) throws FileNotFoundException;
    }

    /**
     * Scale a bitmap, from a {@link File} to a specific height and width
     * @param imageFile a file who's content can be decoded into a {@link Bitmap}
     * @param targetW the desired image width
     * @param targetH the desired image height
     * @return a scaled version of the {@link Bitmap} decoded from the {@link File}
     * @throws FileNotFoundException
     */
    public static Bitmap scaleBitmapImage(final File imageFile, int targetW, int targetH) throws FileNotFoundException {
        if (imageFile == null
                || !imageFile.exists()) {
            throw new FileNotFoundException("File not found");
        }
        Decoder decoder = new Decoder() {
            @Override
            public Bitmap decode(BitmapFactory.Options bmOptions) {
                return BitmapFactory.decodeFile(imageFile.getAbsolutePath(), bmOptions);
            }
        };
        return scaleBitmapImage(decoder, targetW, targetH);
    }

    /**
     * Scale a bitmap, from a {@link Uri} to a specific height and width
     * @param context used to resolve the streamUri using getContentResolver()
     * @param streamUri a uri to a stream which can be decoded into a {@link Bitmap}
     * @param targetW the desired image width
     * @param targetH the desired image height
     * @return a scaled version of the {@link Bitmap} decoded from the stream
     * @throws FileNotFoundException
     */
    public static Bitmap scaleBitmapImage(final Context context, final Uri streamUri, int targetW, int targetH) throws FileNotFoundException {
        if (streamUri == null) {
            throw new FileNotFoundException("File not found");
        }
        Decoder decoder = new Decoder() {
            @Override
            public Bitmap decode(BitmapFactory.Options bmOptions) throws FileNotFoundException {
                return BitmapFactory.decodeStream(context.getContentResolver().openInputStream(streamUri), null, bmOptions);
            }
        };
        return scaleBitmapImage(decoder, targetW, targetH);
    }
    
    private static Bitmap scaleBitmapImage(Decoder decoder, int targetW, int targetH) throws FileNotFoundException {
        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;

        decoder.decode(bmOptions);

        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;

        // Determine how much to scale down the image
        int scaleFactor = Math.min(photoW / targetW, photoH / targetH);

        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        return decoder.decode(bmOptions);
    }

    /**
     * Get a file path from a Uri. This will get the the path for Storage Access
     * Framework Documents, as well as the _data field for the MediaStore and
     * other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @author paulburke
     */
    @SuppressLint("NewApi")
    public static String getPath(final Context context, final Uri uri) {

        final boolean isKitKat = Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT;

        // DocumentProvider
        if (isKitKat && DocumentsContract.isDocumentUri(context, uri)) {
            // ExternalStorageProvider
            if (isExternalStorageDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                if ("primary".equalsIgnoreCase(type)) {
                    return Environment.getExternalStorageDirectory() + "/" + split[1];
                }

                // TODO handle non-primary volumes
            }
            // DownloadsProvider
            else if (isDownloadsDocument(uri)) {

                final String id = DocumentsContract.getDocumentId(uri);
                final Uri contentUri = ContentUris.withAppendedId(
                        Uri.parse("content://downloads/public_downloads"), Long.valueOf(id));

                return getDataColumn(context, contentUri, null, null);
            }
            // MediaProvider
            else if (isMediaDocument(uri)) {
                final String docId = DocumentsContract.getDocumentId(uri);
                final String[] split = docId.split(":");
                final String type = split[0];

                Uri contentUri = null;
                if ("image".equals(type)) {
                    contentUri = MediaStore.Images.Media.EXTERNAL_CONTENT_URI;
                } else if ("video".equals(type)) {
                    contentUri = MediaStore.Video.Media.EXTERNAL_CONTENT_URI;
                } else if ("audio".equals(type)) {
                    contentUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
                }

                final String selection = "_id=?";
                final String[] selectionArgs = new String[]{
                        split[1]
                };

                return getDataColumn(context, contentUri, selection, selectionArgs);
            }
        }
        // MediaStore (and general)
        else if ("content".equalsIgnoreCase(uri.getScheme())) {
            // Return the remote address
            if (isGooglePhotosUri(uri)) {
                return uri.getLastPathSegment();
            }
            return getDataColumn(context, uri, null, null);
        }
        // File
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return uri.toString();
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is Google Photos.
     */
    public static boolean isGooglePhotosUri(Uri uri) {
        return uri.getAuthority().contains("com.google.android.apps.photos");
    }

    /**
     * Get the value of the data column for this Uri. This is useful for
     * MediaStore Uris, and other file-based ContentProviders.
     *
     * @param context The context.
     * @param uri The Uri to query.
     * @param selection (Optional) Filter used in the query.
     * @param selectionArgs (Optional) Selection arguments used in the query.
     * @return The value of the _data column, which is typically a file path.
     */
    public static String getDataColumn(Context context, Uri uri, String selection,
                                       String[] selectionArgs) {

        Cursor cursor = null;
        final String column = "_data";
        final String[] projection = {
                column
        };

        try {
            cursor = context.getContentResolver().query(uri, projection, selection, selectionArgs,
                    null);
            if (cursor != null && cursor.moveToFirst()) {
                final int column_index = cursor.getColumnIndexOrThrow(column);
                return cursor.getString(column_index);
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return null;
    }


    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is ExternalStorageProvider.
     */
    public static boolean isExternalStorageDocument(Uri uri) {
        return "com.android.externalstorage.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is DownloadsProvider.
     */
    public static boolean isDownloadsDocument(Uri uri) {
        return "com.android.providers.downloads.documents".equals(uri.getAuthority());
    }

    /**
     * @param uri The Uri to check.
     * @return Whether the Uri authority is MediaProvider.
     */
    public static boolean isMediaDocument(Uri uri) {
        return "com.android.providers.media.documents".equals(uri.getAuthority());
    }

    /**
     * Checks an image's metadata to determine the desired orientation, and rotates the bitmap directly.
     * @param bitmap the bitmap to rotate
     * @param uri the uri the bitmap was obtained from
     * @return the original bitmap, rotated if needed
     */
    public static Bitmap rotateImageIfNeeded(Context context, Bitmap bitmap, Uri uri) {

        int orientation = getImageRotation(context, uri);

        if (orientation != ExifInterface.ORIENTATION_UNDEFINED) {
            bitmap = rotateBitmap(bitmap, orientation);
        } else {
            orientation = getRotationFromMediaStore(context, uri);
            Matrix matrix = new Matrix();
            matrix.postRotate(orientation);
            bitmap = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, false);
        }

        return bitmap;
    }

    private static int getImageRotation(Context context, Uri imageUri) {
        try {
            ExifInterface exif = new ExifInterface(getPath(context, imageUri));

            return exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, ExifInterface.ORIENTATION_UNDEFINED);
        } catch (IOException e) {
            return 0;
        }
    }

    private static int getRotationFromMediaStore(Context context, Uri imageUri) {
        String[] columns = {MediaStore.Images.ImageColumns.ORIENTATION};
        Cursor cursor;
        try {
            cursor = context.getContentResolver().query(imageUri, columns, null, null, null);
        } catch (IllegalArgumentException e) {
            return 0;
        }
        if (cursor == null) {
            return 0;
        }

        cursor.moveToFirst();

        int orientationColumnIndex = cursor.getColumnIndex(columns[0]);
        int orientation = cursor.getInt(orientationColumnIndex);
        cursor.close();
        return orientation;
    }

    /**
     * Rotate an image to a given rotation
     * @param bitmap the image to rotate
     * @param orientation the desired orientation, must a constant from {@link ExifInterface}
     * @return the original bitmap in the given orientation, if recognised
     */
    public static Bitmap rotateBitmap(Bitmap bitmap, int orientation) {

        Matrix matrix = new Matrix();
        switch (orientation) {
            case ExifInterface.ORIENTATION_NORMAL:
                return bitmap;
            case ExifInterface.ORIENTATION_FLIP_HORIZONTAL:
                matrix.setScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_180:
                matrix.setRotate(180);
                break;
            case ExifInterface.ORIENTATION_FLIP_VERTICAL:
                matrix.setRotate(180);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_TRANSPOSE:
                matrix.setRotate(90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_90:
                matrix.setRotate(90);
                break;
            case ExifInterface.ORIENTATION_TRANSVERSE:
                matrix.setRotate(-90);
                matrix.postScale(-1, 1);
                break;
            case ExifInterface.ORIENTATION_ROTATE_270:
                matrix.setRotate(-90);
                break;
            default:
                return bitmap;
        }

        Bitmap bmRotated = Bitmap.createBitmap(bitmap, 0, 0, bitmap.getWidth(), bitmap.getHeight(), matrix, true);
        bitmap.recycle();
        return bmRotated;
    }
}
