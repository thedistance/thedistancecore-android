package uk.co.thedistance.thedistancecore;

import android.support.annotation.NonNull;

import org.reactivestreams.Subscriber;
import org.reactivestreams.Subscription;

import io.reactivex.functions.Consumer;

public class TDSubscribers {

    /**
     * Create a {@link Subscriber} which only cares about emitted values
     *
     * @param onNext the action to be called when a value is emitted
     * @return The {@link Subscriber}, passing on emitted values and ignoring errors
     */
    public static <T> Subscriber<T> ignorant(@NonNull
                                             final Consumer<T> onNext) {
        return new Subscriber<T>() {

            @Override
            public void onError(Throwable e) {

            }

            @Override
            public void onComplete() {

            }

            @Override
            public void onSubscribe(Subscription s) {

            }

            @Override
            public void onNext(T t) {
                try {
                    onNext.accept(t);
                } catch (Exception e) {
                    onError(e);
                }
            }
        };
    }
}
