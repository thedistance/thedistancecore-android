/*
 * Copyright (c) The Distance Agency Ltd 2016.
 */

package uk.co.thedistance.thedistancecore.intents;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.net.Uri;

import java.util.List;

public class IntentHelper {

    /**
     * Determine whether the specified intent can be handled by the system.
     * e.g. ACTION_DIAL probably can't be handled on a tablet device with no GSM
     * @param context
     * @param intent
     * @return true if the intent can be handled, false otherwise
     */
    public static boolean canSystemHandleIntent(Context context, Intent intent){
        PackageManager manager = context.getPackageManager();
        List<ResolveInfo> infos = manager.queryIntentActivities(intent, 0);
        return infos.size() > 0;
    }

    /**
     * Open a URL in the current app with ACTION_VIEW if possible, else open with standard Intent
     * @param url
     * @param context
     */
    public static void openUrl(String url, Context context) {
        Uri uri = Uri.parse(url);
        Intent intent = new Intent(Intent.ACTION_VIEW, uri);

        final List<ResolveInfo> list = context.getPackageManager().queryIntentActivities(intent, PackageManager.MATCH_DEFAULT_ONLY);
        final String thisApplication = context.getApplicationInfo().packageName;
        for (ResolveInfo i : list) {
            if (thisApplication.equals(i.activityInfo.applicationInfo.packageName)) {
                intent.setComponent(new ComponentName(i.activityInfo.applicationInfo.packageName, i.activityInfo.name));
                break;
            }
        }
        context.startActivity(intent);
    }
}
