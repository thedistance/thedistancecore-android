package uk.co.thedistance.thedistancecore;


import io.reactivex.Observer;
import io.reactivex.disposables.Disposable;

public class TDObservers {

    private static final Observer<Object> EMPTY = new Observer<Object>() {
        @Override
        public void onSubscribe(Disposable d) {

        }

        @Override
        public void onNext(Object value) {

        }

        @Override
        public void onError(Throwable e) {

        }

        @Override
        public void onComplete() {

        }
    };

    /**
     * Returns an inert {@link Observer} that does nothing in response to the emissions or notifications from
     * any {@code Observable} it subscribes to
     *
     * @return an inert {@code Observer}
     */
    @SuppressWarnings("unchecked")
    public static <T> Observer<T> empty() {
        return (Observer<T>) EMPTY;
    }
}
