package uk.co.thedistance.thedistancecore.animation;

import android.animation.Animator;
import android.support.annotation.AnimRes;
import android.support.annotation.IntDef;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewCompat;
import android.view.View;
import android.view.ViewAnimationUtils;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import uk.co.thedistance.thedistancecore.R;
import uk.co.thedistance.thedistancecore.Version;


/**
 * Helper class for providing simple view animations
 */
public class AnimationHelper {

    /**
     * Simple callback for detecting animation start and end. This can be useful for performing
     * an action after running an animation, chaining animations or adding / removing hardware layers
     */
    public interface Callback {
        void onAnimationStart();

        void onAnimationEnd();
    }

    /**
     * Build and execute an animation on a view
     */
    public static class Builder {
        private static final int TYPE_IN = 0;
        private static final int TYPE_OUT = 1;
        private static final int TYPE_OUT_GONE = 2;
        private boolean useHardwareLayers;

        @Retention(RetentionPolicy.SOURCE)
        @IntDef({TYPE_IN, TYPE_OUT, TYPE_OUT_GONE})
        private @interface AnimationType {
        }

        int animId = -1;
        int duration = -1;
        int delay = 0;
        @Builder.AnimationType int type = TYPE_IN;

        /**
         * Provide a custom animation. The default is a simple fade animation.
         *
         * @return the current {@link AnimationHelper.Builder} instance
         */
        public Builder animation(@AnimRes int animId) {
            this.animId = animId;
            return this;
        }

        /**
         * Provide a custom animation duration, in milliseconds. The default is android.R.integer.config_mediumAnimTime.
         *
         * @return the current {@link AnimationHelper.Builder} instance for method chaining
         */
        public Builder duration(int duration) {
            this.duration = duration;
            return this;
        }

        /**
         * Delay the animation start. Default is no delay.
         *
         * @return the current {@link AnimationHelper.Builder} instance for method chaining
         */
        public Builder delay(int delay) {
            this.delay = delay;
            return this;
        }

        /**
         * Execute the animation
         *
         * @param view the view to animate
         */
        public void animate(@NonNull View view) {
            animate(view, null);
        }

        /**
         * Call this method to specify that your view should disappear after animating
         * View will be set to View.GONE after animation
         *
         * @return the current {@link AnimationHelper.Builder} instance
         */
        public Builder out() {
            return out(true);
        }

        /**
         * Call this method to specify that your view should disappear after animating
         *
         * @param gone set the layout to View.GONE after animation
         * @return the current {@link AnimationHelper.Builder} instance
         */
        public Builder out(boolean gone) {
            type = gone ? TYPE_OUT_GONE : TYPE_OUT;
            return this;
        }

        /**
         * Calling this method will automatically set a hardware layer on the animating view,
         * removing it on completion
         *
         * @return the current {@link AnimationHelper.Builder} instance for method chaining
         */
        public Builder useHardwareLayer() {
            useHardwareLayers = true;
            return this;
        }

        /**
         * Execute the animation
         *
         * @param view     the view to animate
         * @param callback a simple callback to detect the animation end
         */
        public void animate(@NonNull View view, @Nullable Callback callback) {
            if (duration == -1) {
                duration = view.getResources().getInteger(android.R.integer.config_mediumAnimTime);
            }
            if (animId == -1) {
                if (type == TYPE_OUT || type == TYPE_OUT_GONE) {
                    animId = R.anim.fade_out;
                } else {
                    animId = R.anim.fade_in;
                }
            }

            if (useHardwareLayers) {
                final Callback userCallback = callback;
                callback = new HardwareLayerCallback(userCallback, view);
            }

            switch (type) {
                case TYPE_OUT:
                case TYPE_OUT_GONE:
                    animateOut(view, animId, duration, delay, callback, type == TYPE_OUT_GONE);
                    break;
                case TYPE_IN:
                    animateIn(view, animId, duration, delay, callback);
                    break;
            }
        }

        private static class HardwareLayerCallback implements Callback {
            private final Callback userCallback;
            private final View view;

            public HardwareLayerCallback(@Nullable Callback userCallback, @NonNull View view) {
                this.userCallback = userCallback;
                this.view = view;
            }

            @Override
            public void onAnimationStart() {
                if (userCallback != null) {
                    userCallback.onAnimationStart();
                }
                view.setLayerType(View.LAYER_TYPE_HARDWARE, null);

            }

            @Override
            public void onAnimationEnd() {
                if (userCallback != null) {
                    userCallback.onAnimationEnd();
                }

                if (ViewCompat.isAttachedToWindow(view)) {
                    view.setLayerType(View.LAYER_TYPE_NONE, null);
                }
            }
        }

    }

    /**
     * Run a circular reveal animation, falling back to a simple fade in on older platforms
     *
     * @param view     the view to animate
     * @param duration the duration of the reveal animation, in milliseconds
     * @param delay    how long to wait before animating, in milliseconds
     */
    public static void circularReveal(final View view, int duration, int delay) {
        if (Version.isLollipop()) {
            int radius = Math.max(view.getWidth(), view.getHeight());
            int cx = view.getWidth() / 2;
            int cy = view.getHeight() / 2;

            Animator animator = ViewAnimationUtils.createCircularReveal(view, cx, cy, 0, radius);
            animator.setDuration(duration);
            animator.setStartDelay(delay);
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    view.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {

                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            animator.start();
        } else {
            fadeIn(view, duration, delay);
        }
    }

    /**
     * Run a circular hide animation, falling back to a simple fade out on older platforms
     *
     * @param view     the view to animate
     * @param duration the duration of the reveal animation, in milliseconds
     * @param delay    how long to wait before animating, in milliseconds
     */
    public static void circularHide(final View view, int duration, int delay) {
        if (Version.isLollipop()) {
            int cx = view.getWidth() / 2;
            int cy = view.getHeight() / 2;
            int radius = Math.max(view.getWidth(), view.getHeight());
            Animator animator = ViewAnimationUtils.createCircularReveal(view, cx, cy, radius, 0);
            animator.setDuration(duration);
            animator.setStartDelay(delay);
            animator.addListener(new Animator.AnimatorListener() {
                @Override
                public void onAnimationStart(Animator animation) {
                    view.setVisibility(View.VISIBLE);
                }

                @Override
                public void onAnimationEnd(Animator animation) {
                    view.setVisibility(View.INVISIBLE);
                }

                @Override
                public void onAnimationCancel(Animator animation) {

                }

                @Override
                public void onAnimationRepeat(Animator animation) {

                }
            });
            animator.start();
        } else {
            fadeOut(view, duration, delay);
        }
    }

    private static void animateIn(final View view, @AnimRes int anim, int duration, int delay, @Nullable final Callback callback) {
        Animation animation = AnimationUtils.loadAnimation(view.getContext(), anim);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if (callback != null) {
                    callback.onAnimationStart();
                }
                view.setVisibility(View.VISIBLE);
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                if (callback != null) {
                    callback.onAnimationEnd();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        if (duration != 0) {
            animation.setDuration(duration);
        }
        animation.setStartOffset(delay);
        view.startAnimation(animation);
    }

    private static void animateOut(final View view, @AnimRes int anim, int duration, int delay, @Nullable final Callback callback, final boolean gone) {
        Animation animation = AnimationUtils.loadAnimation(view.getContext(), anim);
        animation.setAnimationListener(new Animation.AnimationListener() {
            @Override
            public void onAnimationStart(Animation animation) {
                if (callback != null) {
                    callback.onAnimationStart();
                }
            }

            @Override
            public void onAnimationEnd(Animation animation) {
                view.setVisibility(gone ? View.GONE : View.INVISIBLE);
                if (callback != null) {
                    callback.onAnimationEnd();
                }
            }

            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        });
        if (duration != 0) {
            animation.setDuration(duration);
        }
        animation.setStartOffset(delay);
        view.startAnimation(animation);
    }

    /**
     * Run a simple fade in animation
     *
     * @param view     the view to animate
     * @param duration the duration of the reveal animation, in milliseconds
     * @param delay    how long to wait before animating, in milliseconds
     */
    public static void fadeIn(final View view, int duration, int delay) {
        animateIn(view, R.anim.fade_in, duration, delay, null);
    }

    /**
     * Run a simple fade out animation
     *
     * @param view     the view to animate
     * @param duration the duration of the reveal animation, in milliseconds
     * @param delay    how long to wait before animating, in milliseconds
     */
    public static void fadeOut(final View view, int duration, int delay) {
        animateOut(view, R.anim.fade_out, duration, delay, null, true);
    }
}
