package uk.co.thedistance.thedistancecore.text;

import android.text.TextUtils;

import java.util.List;

public class StringUtils {

    /**
     * Append the given separator String to the StringBuilder if not empty.
     *
     * @param stringBuilder The builder to append
     * @param separator     The String to be appended
     */
    public static void separator(StringBuilder stringBuilder, String separator) {
        if (stringBuilder.length() > 0) {
            stringBuilder.append(separator);
        }
    }

    /**
     * Append ", " to a StringBuilder, if not empty
     *
     * @param stringBuilder The builder to append
     */
    public static void comma(StringBuilder stringBuilder) {
        separator(stringBuilder, ", ");
    }

    /**
     * Join a list of objects together in a separated String, using the toString() method for each objects
     *
     * @param objects   The objects to joined together
     * @param separator The String to be used to separate objects
     * @return The separated String
     */
    public static String join(List<?> objects, String separator) {
        if (objects.isEmpty()) {
            return "";
        }
        StringBuilder builder;
        if (objects.get(0) != null) {
            builder = new StringBuilder(objects.size() * objects.get(0).toString().length());
        } else {
            builder = new StringBuilder();
        }
        for (Object object : objects) {
            if (object == null) {
                continue;
            }
            String objString = object.toString();
            if (TextUtils.isEmpty(objString)) {
                continue;
            }
            separator(builder, separator);
            builder.append(objString);
        }
        return builder.toString();
    }

    /**
     * Join an array of objects together in a separated String, using the toString() method for each objects
     *
     * @param separator The String to be used to separate objects
     * @param objects   The objects to joined together
     * @return The separated String
     */
    public static String join(String separator, Object... objects) {
        if (objects.length == 0) {
            return "";
        }
        StringBuilder builder;
        if (objects[0] != null) {
            builder = new StringBuilder(objects.length * objects[0].toString().length());
        } else {
            builder = new StringBuilder();
        }
        for (Object object : objects) {
            if (object == null) {
                continue;
            }
            String objString = object.toString();
            if (TextUtils.isEmpty(objString)) {
                continue;
            }
            separator(builder, separator);
            builder.append(objString);
        }
        return builder.toString();
    }

    /**
     * Join a list of objects together in a separated String, sentence style, using the toString() method for each objects
     *
     * @param objects   The objects to joined together
     * @return The separated String
     */
    public static String sentence(List<?> objects) {
        if (objects.isEmpty()) {
            return "";
        }
        StringBuilder builder;
        if (objects.get(0) != null) {
            builder = new StringBuilder(objects.size() * objects.get(0).toString().length());
        } else {
            builder = new StringBuilder();
        }
        for (int i = 0; i < objects.size(); i++) {
            Object object = objects.get(i);
            if (object == null) {
                continue;
            }
            String objString = object.toString();
            if (TextUtils.isEmpty(objString)) {
                continue;
            }
            separator(builder, i == objects.size() - 1 ? " and " : ",");
            builder.append(objString);
        }
        return builder.toString();
    }
}
