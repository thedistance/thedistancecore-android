package uk.co.thedistance.thedistancecore.intents;

import android.content.Context;
import android.content.Intent;
import android.databinding.BindingAdapter;
import android.net.Uri;
import android.view.View;

/**
 * Custom {@link BindingAdapter} methods relating to intents
 */
public class Bindings {

    /**
     * Adds a {@link View.OnClickListener} for the given phone number
     * @param view      The view to add a listener to
     * @param number    The number to be sent in the ACTION_DIAL intent
     */
    @BindingAdapter({"bind:phone"})
    public static void dialNumber(final View view, final String number) {
        final Context context = view.getContext();
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
                intent.setData(Uri.parse("tel:" + number));
                context.startActivity(Intent.createChooser(intent, "Open with..."));
            }
        });
    }

    /**
     * Adds an {@link View.OnClickListener} for the given email address
     *
     * @param view      The view to add a listener to
     * @param address   The email address to be sent in the ACTION_VIEW intent
     */
    @BindingAdapter({"bind:mail"})
    public static void mailTo(final View view, final String address) {
        final Context context = view.getContext();
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("mailto:" + address));
                context.startActivity(Intent.createChooser(intent, "Open with..."));
            }
        });
    }

    /**
     * Adds an {@link View.OnClickListener} for the given url
     *
     * @param view  The view to add a listener to
     * @param url   The url to be opened
     */
    @BindingAdapter({"bind:urlClick"})
    public static void openUrl(final View view, final String url) {
        final Context context = view.getContext();
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(url));
                context.startActivity(intent);
            }
        });
    }


    /**
     * Adds an {@link View.OnClickListener} for a simple share action, using Intent.createChooser()
     * @param view      The view to add a listener to
     * @param shareable
     */
    @BindingAdapter("bind:shareable")
    public static void share(final View view, final Shareable shareable) {
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent shareIntent = new Intent(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareable.getSharingUrl());
                view.getContext().startActivity(Intent.createChooser(shareIntent, "Share with..."));
            }
        });
    }

    public interface Shareable {
        String getSharingUrl();

        String getTitle();
    }
}
