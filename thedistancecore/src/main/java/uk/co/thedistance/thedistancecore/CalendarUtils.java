package uk.co.thedistance.thedistancecore;

import android.support.annotation.NonNull;

import java.util.Calendar;

/**
 * Helper methods relating to {@link Calendar} objects and dates
 */
public class CalendarUtils {

    /**
     * Set time to midnight, to be used when you just care about the date portion
     *
     * @param cal The Calendar on which to act
     */
    public static void resetTime(@NonNull Calendar cal) {
        cal.set(Calendar.HOUR, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
    }

    /**
     * Convenience method to set the year, month and day of a calendar
     *
     * @param cal         The Calender on which to act
     * @param year        Set to this year
     * @param monthOfYear Set to this month
     * @param dayOfMonth  Set to this day
     * @param resetTime   If true, the time will be set to midnight 0:00:00
     * @return the original calendar, for chaining
     */
    public static Calendar setDate(@NonNull Calendar cal, int year, int monthOfYear, int dayOfMonth, boolean resetTime) {
        if (resetTime) {
            resetTime(cal);
        }
        cal.set(Calendar.YEAR, year);
        cal.set(Calendar.MONTH, monthOfYear);
        cal.set(Calendar.DAY_OF_MONTH, dayOfMonth);
        return cal;
    }
}
